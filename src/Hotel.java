import java.util.ArrayList;

public class Hotel {

    // some basic details about the Hotel
    private String name;
    private String telNo;
    private String email;

    private ArrayList<Room> rooms;
    private ArrayList<Guest> guests;
    private ArrayList<Invoice> invoices;
    private ArrayList<Reservation> reservations;

    public Hotel(String name, String telNo, String email) {
        this.name = name;
        this.telNo = telNo;
        this.email = email;
    }
}
